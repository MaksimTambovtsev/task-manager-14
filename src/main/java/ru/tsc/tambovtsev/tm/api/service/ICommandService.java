package ru.tsc.tambovtsev.tm.api.service;

import ru.tsc.tambovtsev.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}